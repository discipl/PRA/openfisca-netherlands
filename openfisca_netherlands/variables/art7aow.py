"""Article 7 AOW (Dutch general old age (pension) law) variables"""

# Import stuff
from datetime import date
import numpy as np

# Import from openfisca-core the Python objects used to code the legislation in OpenFisca
from openfisca_core.periods import MONTH, ETERNITY
from openfisca_core.variables import Variable
from openfisca_core.populations.config import ADD
from openfisca_netherlands.entities import Person


# custom demographics with precision in months


# birth date
class geboortedatum(Variable):
    value_type = date
    default_value = date(1970, 1, 1)  # By default, if no value is set for a simulation, we consider the people involved in a simulation to be born on the 1st of Jan 1970.
    entity = Person
    definition_period = ETERNITY  # constante
    label = "geboortedatum van een persoon"
    reference = "https://nl.wikipedia.org/wiki/Geboortedatum"

# age in months
class leeftijd(Variable):
    value_type = int
    entity = Person  # heeft degene
    definition_period = MONTH  # we willen de leeftijd in maanden omdat de pensioengerechtigde leeftijd ook in (kalender)maanden is gedefinieerd
    label = "leeftijd van degene"
    reference = "'degene .. leeftijd heeft' https://wetten.overheid.nl/jci1.3:c:BWBR0002221&hoofdstuk=III&paragraaf=1&artikel=7&z=2024-01-01&g=2024-01-01"

    def formula(persons, period):
        geboortedatum = persons("geboortedatum", period)
        year, month, _ = period.start
        return np.datetime64(f"{year:04d}-{month:02d}", "M") - geboortedatum.astype('datetime64[M]')


# end custom demographics


# eligibility for old age pension
class recht_op_ouderdomspensioen(Variable):
    value_type = bool
    entity = Person  # heeft degene
    definition_period = MONTH  # hangt af van leeftijd in maanden dus maand
    label = "Recht op ouderdomspensioen volgens de bepalingen van deze wet"
    reference = "https://wetten.overheid.nl/jci1.3:c:BWBR0002221&hoofdstuk=III&paragraaf=1&artikel=7&z=2024-01-01&g=2024-01-01"

    def formula(persons, period):
        heeft_de_pensioengerechtigde_leeftijd_bereikt = persons('heeft_de_pensioengerechtigde_leeftijd_bereikt', period)
        minimaal_een_jaar_verzekerd_geweest = persons('leeftijd', period) > 0  # defaults to true from birth
        return heeft_de_pensioengerechtigde_leeftijd_bereikt * minimaal_een_jaar_verzekerd_geweest


# has an eligible age for old age pension (depending on age defined in parameter "pgl")
class heeft_de_pensioengerechtigde_leeftijd_(Variable):
    value_type = bool
    entity = Person
    definition_period = MONTH  # hangt af van leeftijd in maanden dus maand
    label = "heeft de pensioengerechtigde leeftijd die geldt op gegeven datum ongeacht of op eerdere datum al bereikt"
    reference = "https://wetten.overheid.nl/jci1.3:c:BWBR0002221&hoofdstuk=III&paragraaf=1&artikel=7&z=2024-01-01&g=2024-01-01"

    def formula(persons, period, parameters):
        heeft_de_pensioengerechtigde_leeftijd_ = persons('leeftijd', period) >= parameters(period).art7aow.pgl
        return heeft_de_pensioengerechtigde_leeftijd_


# has reached the eligible age for old age pension now or before
class heeft_de_pensioengerechtigde_leeftijd_bereikt(Variable):
    value_type = bool
    entity = Person
    definition_period = MONTH  # hangt af van leeftijd in maanden dus maand
    label = "heeft eerder de pensioengerechtigde leeftijd voor het eerst bereikt zoals die gold op die datum"
    reference = "eenmaal bereikt dan is dat voor altijd zo  https://wetten.overheid.nl/jci1.3:c:BWBR0002221&hoofdstuk=III&paragraaf=1&artikel=7&z=2024-01-01&g=2024-01-01"

    # https://wetten.overheid.nl/jci1.3:c:BWBR0002221&hoofdstuk=III&paragraaf=1&artikel=7a&z=2024-01-01&g=2024-01-01
    # stelt ook : "Op pensioengerechtigden die in een bepaald kalenderjaar de pensioengerechtigde leeftijd hebben bereikt zijn de pensioengerechtigde
    # leeftijd en de aanvangsleeftijd in de kalenderjaren daarna niet van toepassing"
    def formula(persons, period):
        return persons('heeft_de_pensioengerechtigde_leeftijd_', period) + persons('heeft_de_pensioengerechtigde_leeftijd_bereikt', period.last_month)


# has not yet reached the eligible age for old age pension now or before since birth
class heeft_niet_de_pensioengerechtigde_leeftijd_bereikt(Variable):
    value_type = bool
    entity = Person
    definition_period = MONTH  # hangt af van leeftijd in maanden dus maand
    label = "heeft eerder de pensioengerechtigde leeftijd voor het eerst bereikt zoals die gold op die datum"
    reference = "eenmaal bereikt dan is dat voor altijd zo  https://wetten.overheid.nl/jci1.3:c:BWBR0002221&hoofdstuk=III&paragraaf=1&artikel=7&z=2024-01-01&g=2024-01-01"

    # https://wetten.overheid.nl/jci1.3:c:BWBR0002221&hoofdstuk=III&paragraaf=1&artikel=7a&z=2024-01-01&g=2024-01-01
    # stelt ook : "Op pensioengerechtigden die in een bepaald kalenderjaar de pensioengerechtigde leeftijd hebben bereikt zijn de pensioengerechtigde
    # leeftijd en de aanvangsleeftijd in de kalenderjaren daarna niet van toepassing"
    def formula(persons, period):
        is_geboren = persons('leeftijd', period) >= 0  # defaults to true from birth
        return np.logical_not(persons('heeft_de_pensioengerechtigde_leeftijd_bereikt', period)) * is_geboren


# amount of months since the eligible age for old age pension has been reached for the first time now or before
class aantal_maanden_pensioen_gerechtigd(Variable):
    value_type = int
    entity = Person
    definition_period = ETERNITY  #
    label = "leeftijd in maanden vanaf wanneer voor het eerst en dus blijvend pensioengerechtigd"
    reference = "https://wetten.overheid.nl/jci1.3:c:BWBR0002221&hoofdstuk=III&paragraaf=1&artikel=7&z=2024-01-01&g=2024-01-01"

    def formula(persons, period):
        return persons('heeft_de_pensioengerechtigde_leeftijd_bereikt', period, options=[ADD])


# amount of months not reaching the eligible age for old age pension for the first time
class aantal_maanden_niet_pensioen_gerechtigd(Variable):
    value_type = int
    entity = Person
    definition_period = ETERNITY  #
    label = "aantal maanden in periode dat de persoon nog niet de pgl heeft bereikt voor het eerst, maanden voor geboorte niet meegerekend"
    reference = "https://wetten.overheid.nl/jci1.3:c:BWBR0002221&hoofdstuk=III&paragraaf=1&artikel=7&z=2024-01-01&g=2024-01-01"

    def formula(persons, period):
        return persons('heeft_niet_de_pensioengerechtigde_leeftijd_bereikt', period, options=[ADD])


# date eligible for old age pension for the first time
class pensioen_gerechtigd_vanaf_datum(Variable):
    value_type = date
    default_value = date(2035, 1, 1)  # By default, if no value is set for a simulation, we consider the people involved in a simulation to be born on the 1st of Jan 1970 and a pensioen gerechtigde leeftijd of 65 years.
    entity = Person
    definition_period = ETERNITY # constante
    label = "pensioen gerechtigd vanaf datum"
    reference = "https://wetten.overheid.nl/jci1.3:c:BWBR0002221&hoofdstuk=III&paragraaf=1&artikel=7&z=2024-01-01&g=2024-01-01"

    def formula(persons, period, parameters):
        pgl = persons('aantal_maanden_niet_pensioen_gerechtigd', period)
        geboortedatum = persons('geboortedatum', period)

        pension_age_in_months = persons('aantal_maanden_niet_pensioen_gerechtigd', period).astype('timedelta64[M]')
        pension_month = geboortedatum.astype('datetime64[M]') + pension_age_in_months
        last_day_of_pension_month = pension_month + np.timedelta64(1, 'M') - np.timedelta64(1, 'D')
        n_days_in_pension_month = last_day_of_pension_month - last_day_of_pension_month.astype('datetime64[M]')
        birth_dayofmonth = geboortedatum - geboortedatum.astype('datetime64[M]') + 1
        pension_dayofmonth = np.where(n_days_in_pension_month < birth_dayofmonth,
                                      n_days_in_pension_month,
                                      birth_dayofmonth
                                      )
        return pension_month.astype('datetime64[D]') + pension_dayofmonth.astype('timedelta64[D]')
